/**
 * The Server Can be configured and created here...
 *
 * You can find the JSON Data file here in the Data module. Feel free to impliment a framework if needed.
 */

/*
-- This is the product data, you can view it in the file itself for more details
{
    "_id": "019",
    "isActive": "false",
    "price": "23.00",
    "picture": "/img/products/N16501_430.png",
    "name": "Damage Reverse Thickening Conditioner",
    "about": "Dolor voluptate velit consequat duis. Aute ad officia fugiat esse anim exercitation voluptate excepteur pariatur sit culpa duis qui esse. Labore amet ad eu veniam nostrud minim labore aliquip est sint voluptate nostrud reprehenderit. Ipsum nostrud culpa consequat reprehenderit.",
    "tags": [
        "ojon",
        "conditioner"
    ]
}
*/
const data      = require('./data');
const http      = require('http');
const hostname  = 'localhost';
const port      = 3035;

const path      = require('path');
const fs      = require('fs');


/**
 * Start the Node Server Here...
 *
 * The http.createServer() method creates a new server that listens at the specified port.
 * The requestListener function (function (req, res)) is executed each time the server gets a request.
 * The Request object 'req' represents the request to the server.
 * The ServerResponse object 'res' represents the writable stream back to the client.
 */
http.createServer(function (req, res) {
    // .. Here you can create your data response in a JSON format
    //correct way - setup proxy
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    const imageApiPath = "/api/img/";
    if (req.url === "/api/cards"){
        let value = '';
        req.on('data', (chunk) => {
            value += chunk;
        });
        req.on('end', () => {

            const result = data.filter(row=>{
                return row.name.toLowerCase().includes(value)||
                    row.tags.find(tag=>tag === value)
                }
            );
            res.write(JSON.stringify(result));
            res.end(); //end the response
        });


    }else if (req.url.startsWith(imageApiPath)){
        const imagePath = req.url.substring(imageApiPath.length);
        let filePath = path.join(__dirname, '../app/images', imagePath);
        let stat
        try{
            stat = fs.statSync(filePath);
        }catch (e) {
            filePath = path.join(__dirname, '../app/images/no-image.png')
            stat = fs.statSync(path.join(__dirname, '../app/images/no-image.png'));
        }

        res.writeHead(200, {
            'Content-Type': 'audio/mpeg',
            'Content-Length': stat.size
        });

        const readStream = fs.createReadStream(filePath);
        // We replaced all the event handlers with a simple call to readStream.pipe()
        readStream.pipe(res);

    }else{
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.write("404 Not Found\n");
        res.end(); //end the response
    }



}).listen( port );


console.log(`[Server running on ${hostname}:${port}]`);
