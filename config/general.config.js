"use strict";
/**
 *
 * File is used to setup the General project
 *
 */

module.exports = {

    api_endpoint:"http://localhost:3035/api",

    development_folder: ".local_server",

    production_folder: "prod",

    image_folder: "img",

    dev_port: 3030

};
