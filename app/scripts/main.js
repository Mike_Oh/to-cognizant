/**
 * The Initial React Setup file
 * ...
 *
 * === CSS
 * The stylesheets are handled seperately using the gulp sass rather than importing them directly into React.
 * You can find these in the ./app/sass/ folder
 *
 * == JS
 * All files in here start from this init point for the React Components.
 *
 *
 * Firstly we need to import the React JS Library
 */
import React from 'react';
import { createRoot } from 'react-dom/client';


import Menu from './components/menu';
import Home from './components/home';
import {api_endpoint} from "../../config/general.config";
import {DEFAULT_DEBOUNCE_TIME} from "../constants";

/**
 * We can start our initial App here in the main.js file
 */
class App extends React.Component {
    cards=[]
    searchValue = ""

    /**
     * Renders the default app in the window, we have assigned this to an element called root.
     *
     * @returns JSX
     * @memberof App
    */
    cancelSearch(){
        this.abortController?.abort();
        this.abortController = undefined;
        clearTimeout(this.timeout);
    }
    doSearch(value, immediately){
        this.cancelSearch();
        const callSearchAPI= async ()=>{
            const abortController = this.abortController = new AbortController();

            try{
                //can be replaced with axios
                const res = await fetch(api_endpoint+"/cards", {
                    method:"post",
                    body:value.toLowerCase(),
                    signal:abortController.signal,
                    headers:{
                        'Content-Type': 'application/json',
                    }});
                if (res.ok){
                    this.searchValue = value;
                    this.cards = await res.json();
                }else{
                    throw new Error("Something went wrong")
                }

            } catch (e) {
                console.log(e);//add popup with error text
                this.searchValue = "error";
                this.cards = [];
            }  finally {
                this.updateHome?.();
            }

        }
        if (immediately){
            callSearchAPI();
        } else{
            this.timeout = setTimeout(callSearchAPI, DEFAULT_DEBOUNCE_TIME);
        }

    }
    /**
     * we need to avoid api call if component unmount
     */
    componentWillUnmount() {
        this.cancelSearch();
    }
    componentDidMount() {
        this.doSearch("",true)
    }

    render() {
        //or React.createContext
        return (
            <div className="App">
                <Menu ctx={this}/>
                <Home ctx={this}/>
            </div>
        );
    }

}

// Render this out
const container = document.getElementById('root');
const root = createRoot(container);
root.render(<App/>);
