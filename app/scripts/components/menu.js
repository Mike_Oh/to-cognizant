/**
 * This file will hold the Menu that lives at the top of the Page, this is all rendered using a React Component...
 *
 */
import React from 'react';
import {DEFAULT_DEBOUNCE_TIME} from "../../constants";
import {api_endpoint} from "../../../config/general.config";

class Menu extends React.Component {

    /**
     * Main constructor for the Menu Class
     * @memberof Menu
     */
    constructor(props) {
        super(props);
        this.ctx = props.ctx;
        this.state = {
            showingSearch: false,
        };
    }

    /**
     * Shows or hides the search container
     * @memberof Menu
     * @param e [Object] - the event from a click handler
     * @param clearSearch
     */
    showSearchContainer(e, clearSearch=false) {
        e.preventDefault();
        if (clearSearch){
            this.inputRef.current.value = ""
            this.ctx.doSearch("", true);
        }
        const showingSearch =  !this.state.showingSearch;
        this.setState({
            showingSearch
        });
        if (showingSearch)this.doFocuseSearch = true;
    }
    inputRef = React.createRef();
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.doFocuseSearch){
            delete this.doFocuseSearch;
            this.inputRef.current.focus();
        }
    }

    /**
     * Calls upon search change
     * @memberof Menu
     * @param e [Object] - the event from a text change handler
     */
    onSearch(e) {
        this.ctx.doSearch(e.target.value)
    }

    /**
     * Renders the default app in the window, we have assigned this to an element called root.
     *
     * @returns JSX
     * @memberof App
    */
    render() {
        return (
            <header className="menu">
                <div className="menu-container">
                    <div className="menu-holder">
                        <h1>ELC</h1>
                        <nav>
                            <a href="#" className="nav-item">HOLIDAY</a>
                            <a href="#" className="nav-item">WHAT'S NEW</a>
                            <a href="#" className="nav-item">PRODUCTS</a>
                            <a href="#" className="nav-item">BESTSELLERS</a>
                            <a href="#" className="nav-item">GOODBYES</a>
                            <a href="#" className="nav-item">STORES</a>
                            <a href="#" className="nav-item">INSPIRATION</a>

                            <a href="#" onClick={(e) => this.showSearchContainer(e)}>
                                <i className="material-icons search">search</i>
                            </a>
                        </nav>
                    </div>
                </div>
                <div className={(this.state.showingSearch ? "showing " : "") + "search-container"}>
                    <input ref={this.inputRef} type="text" onChange={(e) => this.onSearch(e)} />
                    <a href="#" onClick={(e) => {
                        this.showSearchContainer(e, true);
                    }}>
                        <i className="material-icons close">close</i>
                    </a>
                </div>
            </header>
        );
    }


}

// Export out the React Component
export default Menu;
