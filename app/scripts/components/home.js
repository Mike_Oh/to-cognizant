/**
 * This file will hold the Main content that lives in the main body of the site
 *
 */
import React from 'react';
import {api_endpoint} from "../../../config/general.config";


const Home =({ctx})=>{

    const [,setState] = React.useState(0);
    React.useEffect(()=>{
        ctx.updateHome = ()=>{setState(v=>v+1)};
        return ()=>ctx.updateHome = undefined
    },[]);
    const{cards, searchValue} = ctx

    return (
        <section id="home">
            <div className="content">
                {searchValue&&<h2 className="filter">Filtered by: {searchValue}</h2>}
                <div className="cards">
                    {cards.map(({_id, name, about, isActive, price, picture})=>{
                        return <div key={_id} className="card">
                            <img alt="image" src={api_endpoint+picture}/>
                            <div>
                                <h3>{name}</h3>
                                <p>{about}</p>
                                <h1>${price}</h1>
                            </div>

                        </div>

                    })}
                </div>
            </div>
        </section>
    );


}

// Export out the React Component
export default Home;
